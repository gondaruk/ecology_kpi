#!/bin/bash

PID=$(ps auxwww | grep [u]wsgi | grep "[u]wsgi_ecology_kpi.ini" | head -1 | awk '{print $2}')
if [ ! -z "$PID"  ]; then
    kill -9 $PID
fi

uwsgi --ini uwsgi_ecology_kpi.ini

