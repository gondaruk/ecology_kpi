from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin

from django.views.generic import ListView

from dal import autocomplete

from core.models import Region, Company, Emission, Pollutant


class CompanyListView(LoginRequiredMixin, ListView):
    template_name = 'company_map.html'
    model = Company
    context_object_name = 'companies'

    def get_queryset(self):
        qs = super(CompanyListView, self).get_queryset()

        region = self.request.GET.get('region', None)
        if region:
            qs = qs.filter(region__pk=region)
        return qs

    def get_context_data(self, **kwargs):
        context = super(CompanyListView, self).get_context_data(**kwargs)
        context['regions'] = Region.objects.all()

        active_region_pk = self.request.GET.get('region', None)
        context['active_region'] = Region.objects.filter(pk=active_region_pk).first()

        return context


class EmissionListView(LoginRequiredMixin, ListView):
    template_name = 'emissions.html'
    model = Emission
    context_object_name = 'emissions'

    def get_queryset(self):
        qs = super(EmissionListView, self).get_queryset()

        region = self.request.GET.get('region', None)
        if region:
            qs = qs.filter(company__region__pk=region)
        return qs

    def get_context_data(self, **kwargs):
        context = super(EmissionListView, self).get_context_data(**kwargs)
        context['regions'] = Region.objects.all()

        active_region_pk = self.request.GET.get('region', None)
        context['active_region'] = Region.objects.filter(pk=active_region_pk).first()

        return context


class PollutantAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Pollutant.objects.all()
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs

class RegionAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Region.objects.all()
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs
