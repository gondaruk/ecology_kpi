from dal import autocomplete

from django import forms

from core.models import Emission, Pollutant, Region, Company

class EmissionInlineForm(forms.ModelForm):
    pollutant = forms.ModelChoiceField(
        queryset=Pollutant.objects.all(),
        widget=autocomplete.ModelSelect2(url='pollutant-autocomplete')
    )

    class Meta:
        model = Emission
        fields = ('__all__')

class RegionForm(forms.ModelForm):
    region = forms.ModelChoiceField(
        queryset=Region.objects.all(),
        widget=autocomplete.ModelSelect2(url='region-autocomplete')
    )

    class Meta:
        model = Company
        fields = ('__all__')