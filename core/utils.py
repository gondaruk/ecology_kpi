def tpy2gph(v):
    # TonesPerYear --to--> GramPerHour
    return v * 1000000/8760


def gph2tpy(v):
    # GramPerHour --to--> TonesPerYear
    return v / 1000000 * 8760

