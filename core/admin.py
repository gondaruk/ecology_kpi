from django.contrib import admin
from mapwidgets.widgets import GooglePointFieldWidget

from core.models import models, Region, Company, Pollutant, Emission

from core.forms import EmissionInlineForm
from core.forms import RegionForm

admin.site.site_header = 'Ecology KPI'


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ('name', 'author',)


class EmissionInlineAdmin(admin.TabularInline):
    model = Emission
    form = EmissionInlineForm
    fields = ('pollutant', 'emission',)
    extra = 1

@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    form = RegionForm
    inlines = (EmissionInlineAdmin,)
    list_display = ('name', 'region', 'have_coordinates')
    list_filter = ('region',)
    search_fields = ('name',)
    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget}
    }

    def have_coordinates(self, obj):
        return obj.location is not None
    have_coordinates.boolean = True


@admin.register(Pollutant)
class PollutantAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'mass_flow', 'maximum_emission')


@admin.register(Emission)
class EmissionAdmin(admin.ModelAdmin):
    list_display = ('company', 'pollutant', 'emission', 'overemit_percentage')
    list_filter = ('company__region', 'pollutant',)

    def overemit_percentage(self, obj):
        if obj.overemit_percentage:
            return str(round(obj.overemit_percentage, 4)) + ' %'
        return None


