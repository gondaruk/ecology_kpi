from django.contrib import admin
from django.urls import path
from django.views.generic import RedirectView

from core.views import CompanyListView, EmissionListView, PollutantAutocompleteView, RegionAutocompleteView

urlpatterns = [
    path('', RedirectView.as_view(pattern_name='companies', permanent=False), name='index'),
    path('company', CompanyListView.as_view(), name='companies'),
    path('emission', EmissionListView.as_view(), name='emissions'),
    path('autocomplete/pollutant', PollutantAutocompleteView.as_view(), name='pollutant-autocomplete'),
    path('autocomplete/region', RegionAutocompleteView.as_view(), name='region-autocomplete'),
]
