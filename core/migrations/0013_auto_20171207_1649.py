# Generated by Django 2.0 on 2017-12-07 16:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_auto_20171207_1313'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emission',
            name='emission',
            field=models.DecimalField(decimal_places=4, default=0, max_digits=10, verbose_name="Об'єм викидів (г/год)"),
        ),
    ]
