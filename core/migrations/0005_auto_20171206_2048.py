# Generated by Django 2.0 on 2017-12-06 20:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20171206_2044'),
    ]

    operations = [
        migrations.CreateModel(
            name='Emission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('maximum_emission', models.DecimalField(decimal_places=4, default=0, max_digits=10, verbose_name="Об'єм викидів")),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Company')),
            ],
        ),
        migrations.AlterField(
            model_name='pollutant',
            name='mass_flow',
            field=models.DecimalField(decimal_places=4, default=0, help_text='g/h', max_digits=10, verbose_name='Величина масової витрати (г/год)'),
        ),
        migrations.AlterField(
            model_name='pollutant',
            name='maximum_emission',
            field=models.DecimalField(decimal_places=4, default=0, help_text='mg/m3', max_digits=10, verbose_name='Гранично допустимі викиди (мг/м3)'),
        ),
        migrations.AddField(
            model_name='emission',
            name='pollutant',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Pollutant'),
        ),
    ]
