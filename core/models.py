from django.contrib.gis.db import models

from core.utils import gph2tpy


class Region(models.Model):
    name = models.CharField(max_length=512, null=False, blank=True, default='')
    author = models.CharField(max_length=512, null=False, blank=True, default='')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('pk',)


class Company(models.Model):
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    name = models.CharField(max_length=512, null=False, blank=True, default='')
    location = models.PointField(blank=True, null=True, default=None)

    def __str__(self):
        return self.name

    @property
    def is_danger(self):
        res = False
        for emission in self.emission_set.all():
            if emission.overemit_value:
                res = True
                break
        return res

    @property
    def tax_to_pay(self):
        res = 0.0
        for emission in self.emission_set.all():
            if emission.tax_to_pay:
                res += emission.tax_to_pay
        return res

    class Meta:
        order_with_respect_to = 'region'
        verbose_name = 'Company'
        verbose_name_plural = 'Companies'


class Pollutant(models.Model):
    CATEGORY_CLASS_1 = 'Class I'
    CATEGORY_CLASS_2 = 'Class II'
    CATEGORY_CLASS_3 = 'Class III'
    CATEGORY_CLASS_4 = 'Class IV'

    CATEGORY_CHOICES = (
        (CATEGORY_CLASS_1, CATEGORY_CLASS_1),
        (CATEGORY_CLASS_2, CATEGORY_CLASS_2),
        (CATEGORY_CLASS_3, CATEGORY_CLASS_3),
        (CATEGORY_CLASS_4, CATEGORY_CLASS_4),
    )
    name = models.CharField(max_length=512, null=False, blank=True, default='')
    category = models.CharField(max_length=512, choices=CATEGORY_CHOICES, null=True, blank=False, default=None)
    mass_flow = models.DecimalField('Величина масової витрати (г/год)', max_digits=50, decimal_places=4, default=0, help_text='g/h')
    maximum_emission = models.DecimalField('Гранично допустимі викиди (мг/м3)', max_digits=50, decimal_places=4, default=0, help_text='mg/m3')

    tax_rate = models.DecimalField('Ставка податку за тонну (грн)', max_digits=10, decimal_places=2, default=0, help_text='uah')
    about = models.TextField(null=False, blank=True, default='')

    def __init__(self, *args, **kwargs):
        super(Pollutant, self).__init__(*args, **kwargs)
        self._normalize_tax_rate()

    def _normalize_tax_rate(self):
        if self.tax_rate and self.tax_rate > 0:
            return
        if self.category == self.CATEGORY_CLASS_1:
            self.tax_rate = 14080.5
        elif self.category == self.CATEGORY_CLASS_2:
            self.tax_rate = 3224.65
        elif self.category == self.CATEGORY_CLASS_3:
            self.tax_rate = 480.47
        elif self.category == self.CATEGORY_CLASS_4:
            self.tax_rate = 111.26

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('category', 'pk',)


class Emission(models.Model):
    pollutant = models.ForeignKey(Pollutant, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    emission = models.DecimalField('Об\'єм викидів (г/год)', max_digits=50, decimal_places=4, default=0)
    comment = models.TextField(null=False, blank=True, default='')

    @property
    def overemit_value(self):
        if self.emission <= self.pollutant.mass_flow:
            return None
        return self.emission - self.pollutant.mass_flow

    @property
    def overemit_percentage(self):
        if self.overemit_value is None:
            return None
        return self.overemit_value / self.pollutant.mass_flow * 100

    @property
    def tax_to_pay(self):
        if not self.overemit_value:
            return None
        return float(gph2tpy(self.emission)) * float(self.pollutant.tax_rate)



    def __str__(self):
        return '{} from {}'.format(self.pollutant.name, self.company.name)

    class Meta:
        ordering = ('company', 'pk',)
