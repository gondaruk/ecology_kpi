Django==2.0
django-autocomplete-light==3.2.10
django-map-widgets==0.1.9
psycopg2==2.7.3.2
pytz==2017.3
six==1.11.0
